import 'package:flutter/material.dart';

class Items {
  String title;
  String subtitle;
  String event;
  String img;
  Items({this.title, this.subtitle, this.event, this.img});
}

class GridDashboard extends StatelessWidget {
  Items item1 = new Items(
    title: "Handphone",
    // subtitle: "March, Wednesday",
    // event: "3 Events",
    img: "asset/images/supplier.png",
  );

  Items item2 = new Items(
    title: "Laptop",
    // subtitle: "Bocali, Apple",
    // event: "4 Items",
    img: "asset/images/supplier.png",
  );
  Items item3 = new Items(
    title: "Power Bank",
    // subtitle: "Lucy Mao going to Office",
    // event: "",
    img: "asset/images/supplier.png",
  );
  Items item4 = new Items(
    title: "Monitor",
    // subtitle: "Rose favirited your Post",
    // event: "",
    img: "asset/images/supplier.png",
  );

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1, item2, item3, item4];
    var color = 0xff453658;
    return Flexible(
      child: GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return Container(
              decoration: BoxDecoration(
                  color: Color(color), borderRadius: BorderRadius.circular(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    data.img,
                    width: 42,
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Text(
                    data.title,
                    style: TextStyle(
                        color: Colors.white70,
                        fontSize: 11,
                        fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    data.subtitle,
                    style: TextStyle(
                        color: Colors.white70,
                        fontSize: 11,
                        fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Text(
                    data.event,
                    style: TextStyle(
                        color: Colors.white70,
                        fontSize: 11,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            );
          }).toList()),
    );
  }
}
