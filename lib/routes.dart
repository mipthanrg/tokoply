import 'package:flutter/widgets.dart';
import 'package:tokoply/screen/ForgotPassword/forgot_password_screen.dart';
import 'package:tokoply/screen/LoginSucces/login_succes_screen.dart';
import 'package:tokoply/screen/Menu/menu_user_screen.dart';
import 'package:tokoply/screen/Menu_admin/menu_admin_screen.dart';
import 'package:tokoply/screen/Sign%20Up/sign_up_screen.dart';
import 'package:tokoply/screen/Sign_In/sign_in_screen.dart';
import 'package:tokoply/screen/SplashScreen/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  LoginSuccesScreen.routeName: (context) => LoginSuccesScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  MenuUserScreen.routeName: (context) => MenuUserScreen(null),
  MenuAdminScreen.routeName: (context) => MenuAdminScreen(),
};
