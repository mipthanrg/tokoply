import 'package:flutter/material.dart';
import 'package:tokoply/components/default_button.dart';
import 'package:tokoply/screen/Menu/menu_user_screen.dart';
import 'package:tokoply/size_config.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset("asset/images/product.png",
            height: SizeConfig.screenHeight * 0.4),
        // SvgPicture.asset(
        //   "asset/icons/016-delivery van4.svg",
        //   height: SizeConfig.screenHeight * 0.4,
        // ),
        Text(
          "Login Success",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(25),
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        SizedBox(
          child: DefaultButton(
            text: "Back To Home",
            press: () {
              Navigator.pushNamed(context, MenuUserScreen.routeName);
            },
          ),
        ),
        Spacer(),
      ],
    );
  }
}
