import 'package:flutter/material.dart';
import 'package:tokoply/screen/LoginSucces/components/body.dart';

class LoginSuccesScreen extends StatelessWidget {
  static String routeName = "/login_succes";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Success"),
      ),
      body: Body(),
    );
  }
}
