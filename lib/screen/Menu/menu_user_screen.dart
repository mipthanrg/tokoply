import 'package:flutter/material.dart';
import 'package:tokoply/screen/Menu/components/body.dart';

class MenuUserScreen extends StatelessWidget {
  static String routeName = "/menu";
  final VoidCallback logOut;

  MenuUserScreen(this.logOut);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
    );
  }
}
