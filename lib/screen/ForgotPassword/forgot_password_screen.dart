import 'package:flutter/material.dart';
import 'package:tokoply/screen/ForgotPassword/components/body.dart';

class ForgotPasswordScreen extends StatelessWidget {
  static String routeName = "/forgot";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Forgot Password"),
      ),
      body: Body(),
    );
  }
}
