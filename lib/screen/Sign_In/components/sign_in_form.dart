import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tokoply/components/default_button.dart';
import 'package:tokoply/components/form_error.dart';
import 'package:tokoply/constant.dart';
import 'package:tokoply/model/api.dart';
import 'package:tokoply/screen/ForgotPassword/forgot_password_screen.dart';
import 'package:tokoply/screen/Menu/menu_user_screen.dart';
import 'package:tokoply/screen/Menu_admin/menu_admin_screen.dart';
import 'package:tokoply/size_config.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class SignForm extends StatefulWidget {
  SignForm({Key key}) : super(key: key);

  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  bool _secureText = false;
  bool remember = false;

  // Show SecureText
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  Future<List> login() async {
    final response = await http.post(Uri.parse(UrlBase.apiLogin), body: {
      "email": email.text,
      "password": password.text,
    });
    print(response.body);

    var datauser = json.decode(response.body);
    if (datauser["data"]["role"] == 2) {
      print("welcome User");
      Navigator.pushNamed(context, MenuUserScreen.routeName);
    } else if (datauser["data"]["role"] == 1) {
      print("welcome admin");
      Navigator.pushReplacementNamed(context, MenuAdminScreen.routeName);
    }
  }

  @override
  // void initState() {
  //   super.initState();
  //   getPref();
  // }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          SizedBox(height: getProportionateScreenHeight(30)),
          buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPasswordFormField(),
          Row(
            children: [
              Checkbox(
                value: remember,
                activeColor: kPrimaryColor,
                onChanged: (value) {
                  setState(() {
                    remember = value;
                  });
                },
              ),
              Text("Remember me"),
              Spacer(),
              GestureDetector(
                onTap: () => Navigator.pushNamed(
                    context, ForgotPasswordScreen.routeName),
                child: Text(
                  "Forgot Password",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          // Row(
          //   children: [
          //     SvgPicture.asset("asset/icons/Error.svg"),
          //     SizedBox(
          //       height: getProportionateScreenHeight(10),
          //       width: getProportionateScreenWidth(10),
          //     ),
          //   ],
          // ),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(20)),
          DefaultButton(
            text: "Continue",
            press: () {
              login();
              // Navigator.pushNamed(context, LoginSuccesScreen.routeName);
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      controller: password,
      // onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty && errors.contains(kPassNullError)) {
          setState(() {
            errors.remove(kPassNullError);
          });
          return "";
        } else if (value.length >= 8 && errors.contains(kShortPassError)) {
          setState(() {
            errors.remove(kShortPassError);
          });
          return "";
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty && !errors.contains(kPassNullError)) {
          setState(() {
            errors.add(kPassNullError);
          });
        } else if (value.length < 8 && !errors.contains(kShortPassError)) {
          setState(() {
            errors.add(kShortPassError);
          });
          return "";
        }
        return null;
      },
      obscureText: true,
      decoration: InputDecoration(
          labelText: "Password",
          hintText: "Enter Your Password",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: IconButton(
            icon: Icon(_secureText ? Icons.visibility_off : Icons.visibility),
            onPressed: () {
              showHide();
            },
          )
          // suffixIcon: CustomSuffixIcon(
          //   svgIcon: "asset/icons/Lock.svg",
          // ),
          ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      controller: email,
      keyboardType: TextInputType.emailAddress,
      // onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty && errors.contains(kEmailNullError)) {
          setState(() {
            errors.remove(kEmailNullError);
          });
          return "";
        } else if (emailValidatorRegExp.hasMatch(value) &&
            errors.contains(kInvalidEmailError)) {
          setState(() {
            errors.remove(kInvalidEmailError);
          });
          return "";
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty && !errors.contains(kEmailNullError)) {
          setState(() {
            errors.add(kEmailNullError);
          });
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value) &&
            !errors.contains(kInvalidEmailError)) {
          setState(() {
            errors.add(kInvalidEmailError);
          });
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Email",
        hintText: "Enter Your Email",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Padding(
            padding: const EdgeInsetsDirectional.only(end: 12.0),
            child: Icon(Icons.mail)
            // suffixIcon: CustomSuffixIcon(svgIcon: "asset/icons/Mail.svg"),
            ),
        // suffixIcon: CustomSuffixIcon(
        //   svgIcon: "asset/icons/Mail.svg",
        // ),
      ),
    );
  }
}
