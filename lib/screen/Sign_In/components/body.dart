import 'package:flutter/material.dart';
import 'package:tokoply/components/no_account_text.dart';
import 'package:tokoply/components/social_card.dart';
import 'package:tokoply/constant.dart';
import 'package:tokoply/screen/Sign_In/components/sign_in_form.dart';
import 'package:tokoply/size_config.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: SizeConfig.screenHeight * 0.04,
                ),
                Text(
                  "Welcome Back",
                  style: TextStyle(
                      fontSize: getProportionateScreenWidth(28),
                      color: kPrimaryColor,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Sign in with your email and password \nor countinue with social media",
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.03,
                ),
                SignForm(),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.03,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SocialCard(
                      icon: "asset/icons/google-icon.svg",
                      press: () {},
                    ),
                    SocialCard(
                      icon: "asset/icons/facebook-2.svg",
                      press: () {},
                    ),
                    SocialCard(
                      icon: "asset/icons/twitter.svg",
                      press: () {},
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.03,
                ),
                NoAccountText(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
